package com.example.anbello.gttsample.data.model;


import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

public class Station {

    private static final String BUS_TYPE = "BUS";

    private String id;
    private LatLng latLng;
    private String lines;
    private String name;
    private String type;

    public Station(@NonNull JSONObject jsonObject) throws JSONException {
        try {
            id = jsonObject.getString("id");
            double lat = Double.parseDouble(jsonObject.getString("lat"));
            double lng = Double.parseDouble(jsonObject.getString("lng"));
            latLng = new LatLng(lat, lng);
            lines = jsonObject.getString("lines");
            name = jsonObject.getString("name");
            type = jsonObject.getString("type");
        } catch (NumberFormatException e) {
            throw new JSONException(e.getMessage());
        }
    }

    public String getId() {
        return id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public String getLines() {
        return lines;
    }

    public String getName() {
        return name;
    }

    public boolean isBusStatiuon() {
        return  BUS_TYPE.equals(type);
    }
}