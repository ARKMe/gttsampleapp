package com.example.anbello.gttsample.bus_station_selection;


import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.example.anbello.gttsample.data.model.Station;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface BusStationSelectionContract {

    interface View extends com.example.anbello.gttsample.View {
        Activity getActivity();
        void playServicesReady();
        void mapReady(GoogleMap map);
        void showUserPosition(LatLng position);
        void showBusStations(List<Station> stations);
    }

    interface Presenter extends com.example.anbello.gttsample.Presenter, OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
        void getMap(SupportMapFragment mapFragment);
        void permissionResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults);
        boolean onActivityResult(int requestCode, int resultCode, Intent data);
        void getUserPosition();
        void populateBusStations();
    }

}
