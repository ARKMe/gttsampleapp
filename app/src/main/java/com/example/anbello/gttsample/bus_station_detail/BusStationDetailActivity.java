package com.example.anbello.gttsample.bus_station_detail;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.anbello.gttsample.BaseActivity;
import com.example.anbello.gttsample.R;
import com.example.anbello.gttsample.data.model.LineDataForBusStation;

import java.util.List;

public class BusStationDetailActivity extends BaseActivity implements BusStationDetailContract.View {

    public static final String ARG_STATION_ID = BusStationDetailActivity.class.getPackage().getName() + ".ARG_STATION_ID";
    public static final String ARG_STATION_NAME = BusStationDetailActivity.class.getPackage().getName() + ".ARG_STATION_NAME";

    private LinesDataAdapter mAdapter;
    private BusStationDetailPresenter mPresenter;
    private SwipeRefreshLayout mRrefreshLayout;

    @Override
    protected int getLayout() {
        return R.layout.activity_station_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String mStationId = getIntent().getStringExtra(ARG_STATION_ID);
        String mStationName = getIntent().getStringExtra(ARG_STATION_NAME);

        mPresenter = new BusStationDetailPresenter(this);
        mPresenter.start(this);

        mRrefreshLayout = findViewById(R.id.refresher);
        mRrefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.populateLinesData(mStationId, false);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.station_title, mStationId, mStationName));
        RecyclerView recyclerView = findViewById(R.id.list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new LinesDataAdapter();
        recyclerView.setAdapter(mAdapter);

        mPresenter.populateLinesData(mStationId, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.stop();
    }

    @Override
    public void showLinesData(List<LineDataForBusStation> stations) {
        mAdapter.setData(stations);
    }

    @Override
    public void hideLoader() {
        super.hideLoader();
        mRrefreshLayout.setRefreshing(false);
    }

}
