package com.example.anbello.gttsample;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

public abstract class BaseActivity extends FragmentActivity implements View {

    protected android.view.View mLoader;
    protected Toast mToast;

    protected abstract @LayoutRes int getLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        mLoader = findViewById(R.id.loader);
    }

    @Override
    public void showLoader() {
        mLoader.setVisibility(android.view.View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        mLoader.setVisibility(android.view.View.GONE);
    }

    @Override
    public void showError(@StringRes int errorResId) {
        if (mToast != null)
            mToast.cancel();

        mToast = Toast.makeText(this, errorResId, Toast.LENGTH_LONG);
        mToast.show();
    }
}
