package com.example.anbello.gttsample.bus_station_detail;


import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.anbello.gttsample.R;
import com.example.anbello.gttsample.data.model.LineDataForBusStation;
import com.example.anbello.gttsample.network.RestAPI5T;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;


public class BusStationDetailPresenter implements BusStationDetailContract.Presenter {

    private static String TAG = BusStationDetailPresenter.class.getName();

    @NonNull
    private final BusStationDetailContract.View mView;


    public BusStationDetailPresenter(@NonNull BusStationDetailContract.View view) {
        mView = view;
    }

    @Override
    public void start(Context context) {
        RestAPI5T.init(context);
    }

    @Override
    public void stop() {
        RestAPI5T.clear(TAG);
    }

    @Override
    public void populateLinesData(String id, boolean showLoader) {
        if (showLoader)
            mView.showLoader();

        RestAPI5T.getBusStationData(
                id,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<LineDataForBusStation> linesData = new LinkedList<>();
                        for (int i=0; i<response.length(); i++) {
                            try {
                                linesData.add(new LineDataForBusStation(response.optJSONObject(i)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        mView.showLinesData(linesData);
                        mView.hideLoader();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mView.showError(R.string.network_error);
                        mView.hideLoader();
                    }
                },
                TAG);
    }

}
