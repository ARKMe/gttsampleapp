package com.example.anbello.gttsample.bus_station_detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anbello.gttsample.R;
import com.example.anbello.gttsample.data.model.LineDataForBusStation;

import java.util.List;


public class LinesDataAdapter extends RecyclerView.Adapter<LinesDataAdapter.ViewHolder> {

    private List<LineDataForBusStation> mData;

    public void setData(List<LineDataForBusStation> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mShortName;
        TextView mLongName;
        TextView mNextDepartures;

        ViewHolder(View v) {
            super(v);
            mShortName = v.findViewById(R.id.short_name);
            mLongName = v.findViewById(R.id.long_name);
            mNextDepartures = v.findViewById(R.id.next_departures);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LineDataForBusStation element = mData.get(position);
        holder.mShortName.setText(element.getName());
        holder.mLongName.setText(element.getLongName());
        holder.mNextDepartures.setText(element.getFormattedDepartures());
    }

    @Override
    public int getItemCount() {
        if (mData == null)
            return 0;
        return mData.size();
    }
}
