package com.example.anbello.gttsample.bus_station_detail;


import com.example.anbello.gttsample.data.model.LineDataForBusStation;

import java.util.List;


public interface BusStationDetailContract {

    interface View extends com.example.anbello.gttsample.View {
        void showLinesData(List<LineDataForBusStation> stations);
    }

    interface Presenter extends com.example.anbello.gttsample.Presenter {
        void populateLinesData(String id, boolean showLoader);
    }

}
