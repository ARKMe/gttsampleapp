package com.example.anbello.gttsample.data.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Departure {

    private boolean realTime;
    private String time;

    public Departure(JSONObject jsonObject) throws JSONException {
        this.realTime = jsonObject.getBoolean("rt");
        this.time = jsonObject.getString("time");
    }

    public boolean isRealTime() {
        return realTime;
    }

    public String getTime() {
        return time;
    }
}