package com.example.anbello.gttsample.bus_station_selection;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.anbello.gttsample.R;
import com.example.anbello.gttsample.bus_station_detail.BusStationDetailActivity;
import com.example.anbello.gttsample.data.model.Station;
import com.example.anbello.gttsample.network.RestAPI5T;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class BusStationSelectionPresenter implements BusStationSelectionContract.Presenter {

    private final static String TAG = BusStationSelectionPresenter.class.getSimpleName();

    private final static int MY_PERMISSIONS_REQUEST_LOCATION = 42;
    private final static int MY_PLAY_SERVICES_REQUEST_CODE = 42;

    @NonNull
    private final BusStationSelectionContract.View mView;

    private FusedLocationProviderClient fusedLocationClient;

    public BusStationSelectionPresenter(@NonNull BusStationSelectionContract.View view) {
        mView = view;
    }

    @Override
    public void start(Context context) {
        RestAPI5T.init(context);

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int result = googleApiAvailability.isGooglePlayServicesAvailable(mView.getActivity());
        if (result != ConnectionResult.SUCCESS) {
            googleApiAvailability.getErrorDialog(mView.getActivity(), result, MY_PLAY_SERVICES_REQUEST_CODE);
        } else {
            mView.playServicesReady();
        }
    }

    @Override
    public void stop() {
        RestAPI5T.clear(TAG);
    }

    @Override
    public void getMap(SupportMapFragment mapFragment) {
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mView.mapReady(googleMap);
    }

    @Override
    public void permissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != MY_PERMISSIONS_REQUEST_LOCATION)
            return;

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startGeolocalization();
        }
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case MY_PLAY_SERVICES_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    mView.playServicesReady();
                } else {
                    mView.showError(R.string.play_services_error);
                }
                return true;
        }
        return false;
    }

    @Override
    public void getUserPosition() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(mView.getActivity());
        if (ContextCompat.checkSelfPermission(mView.getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    mView.getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);

        } else {
            startGeolocalization();
        }
    }

    @SuppressLint("MissingPermission")
    private void startGeolocalization() {
        fusedLocationClient.getLastLocation().addOnSuccessListener(
                mView.getActivity(),
                new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location == null)
                            return;

                        mView.showUserPosition(new LatLng(
                                location.getLatitude(),
                                location.getLongitude()
                        ));
                    }
                }
        );
    }

    @Override
    public void populateBusStations() {
        mView.showLoader();
        RestAPI5T.getAllBusStation(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        List<Station> res = new LinkedList<>();
                        JSONArray stopsJS = response.optJSONArray("stops");
                        if (stopsJS == null) {
                            mView.showBusStations(res);
                            mView.hideLoader();
                            return;
                        }

                        for (int i=0; i<stopsJS.length(); i++) {
                            try {
                                Station station = new Station(stopsJS.optJSONObject(i));
                                if (station.isBusStatiuon())
                                    res.add(new Station(stopsJS.optJSONObject(i)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        mView.showBusStations(res);
                        mView.hideLoader();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mView.showError(R.string.network_error);
                        mView.hideLoader();
                    }
                },
                TAG);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Object tag = marker.getTag();
        if (!(tag instanceof Station))
            return;

        Station station = (Station) tag;
        Intent intent = new Intent(mView.getActivity(), BusStationDetailActivity.class);
        intent.putExtra(BusStationDetailActivity.ARG_STATION_ID, station.getId());
        intent.putExtra(BusStationDetailActivity.ARG_STATION_NAME, station.getName());
        mView.getActivity().startActivity(intent);
    }

}
