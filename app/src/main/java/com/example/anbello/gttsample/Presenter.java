package com.example.anbello.gttsample;

import android.content.Context;

public interface Presenter {
    void start(Context context);
    void stop();
}
