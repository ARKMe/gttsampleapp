package com.example.anbello.gttsample;


import android.support.annotation.StringRes;

public interface View {
    void showLoader();
    void hideLoader();
    void showError(@StringRes int errorResId);
}
