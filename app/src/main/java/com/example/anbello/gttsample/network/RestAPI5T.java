package com.example.anbello.gttsample.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;


public class RestAPI5T {

    private static final String BASE_URL = "http://www.5t.torino.it/ws2.1/rest/";

    private static RequestQueue queue;

    private RestAPI5T() {}

    public static void init(Context context) {
        if (queue == null)
            queue = Volley.newRequestQueue(context);
    }

    public static void getAllBusStation(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String tag) {
        getObject(BASE_URL + "stops/all", responseListener, errorListener, tag);
    }

    public static void getBusStationData(String stationId, Response.Listener<JSONArray> responseListener, Response.ErrorListener errorListener, String tag) {
        getArray(BASE_URL + "stops/" + stationId + "/departures", responseListener, errorListener, tag);
    }

    private static void getObject(String url, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, String tag) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                responseListener,
                errorListener
        );
        add(jsonObjectRequest, tag);
    }

    private static void getArray(String url, Response.Listener<JSONArray> responseListener, Response.ErrorListener errorListener, String tag) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                responseListener,
                errorListener
        );
        add(jsonArrayRequest, tag);
    }

    private static <T> void add(JsonRequest<T> request, String tag) {
        Log.d("REST", request.getUrl());
        request.setTag(tag);
        queue.add(request);
    }

    public static void clear(String tag) {
        queue.cancelAll(tag);
    }

}