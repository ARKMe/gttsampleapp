package com.example.anbello.gttsample.data.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class LineDataForBusStation {

    private String longName;
    private String name;
    private List<Departure> departures;

    public LineDataForBusStation(JSONObject jsonObject) throws JSONException {
        this.longName = jsonObject.getString("longName");
        this.name = jsonObject.getString("name");
        this.departures = new LinkedList<>();
        JSONArray depsJSON = jsonObject.optJSONArray("departures");
        JSONObject depJSON;

        if (depsJSON == null)
            return;

        for (int i=0; i<depsJSON.length(); i++) {
            try {
                depJSON = depsJSON.getJSONObject(i);
                departures.add(new Departure(depJSON));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getLongName() {
        return longName;
    }

    public String getName() {
        return name;
    }

    public List<Departure> getDepartures() {
        return departures;
    }

    public String getFormattedDepartures() {
        if (departures.size() == 0) {
            return "-";
        }

        StringBuilder stringBuilder = new StringBuilder();
        addFormattedDeparture(stringBuilder, departures.get(0));
        for (int i=1; i<departures.size(); i++) {
            stringBuilder.append(", ");
            addFormattedDeparture(stringBuilder, departures.get(i));
        }
        return stringBuilder.toString();
    }

    private static void addFormattedDeparture(StringBuilder stringBuilder, Departure departure) {
        stringBuilder.append(departure.getTime());
        if (departure.isRealTime())
            stringBuilder.append('*');
    }

}
