package com.example.anbello.gttsample.bus_station_selection;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;

import com.example.anbello.gttsample.BaseActivity;
import com.example.anbello.gttsample.R;
import com.example.anbello.gttsample.data.model.Station;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

public class BusStationSelectionActivity extends BaseActivity implements BusStationSelectionContract.View {

    private final static String MAP_OLD_CENTER = "map_old_center";
    private final static String MAP_OLD_ZOOM = "map_old_zoom";

    private final static LatLng TORINO_LAT_LNG = new LatLng(45.0636276,7.6661333);

    private final static float MIN_ZOOM_LEVEL = 14.5f;
    private final static int ZOOM_LEVEL = 16;

    private GoogleMap mMap;
    private LatLng mPosition;
    private List<Station> mStations;
    private HashMap<String, Marker> mVisibleStations;

    private Float mOldZoom;
    private LatLng mOldCenter;

    BusStationSelectionPresenter mPresenter;
    private SupportMapFragment mMapFragment;

    @Override
    protected int getLayout() {
        return R.layout.activity_bus_station_selection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mOldZoom = savedInstanceState.getFloat(MAP_OLD_ZOOM);
            mOldCenter = savedInstanceState.getParcelable(MAP_OLD_CENTER);
        }

        mVisibleStations = new HashMap<>();
        mPresenter = new BusStationSelectionPresenter(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mPresenter.start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean handled = mPresenter.onActivityResult(requestCode, resultCode, data);
        if (!handled)
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void playServicesReady() {
        mPresenter.getMap(mMapFragment);
    }

    @Override
    public void mapReady(GoogleMap map) {
        mMap = map;
        mMap.setMinZoomPreference(MIN_ZOOM_LEVEL);
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                updateMarkers();
            }
        });

        mMap.moveCamera(CameraUpdateFactory.zoomTo(mOldZoom == null ? ZOOM_LEVEL : mOldZoom));
        moveCamera(mOldCenter == null ? TORINO_LAT_LNG : mOldCenter);

        mPresenter.populateBusStations();
        mPresenter.getUserPosition();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPresenter.permissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void showBusStations(List<Station> stations) {
        mStations = stations;
        updateMarkers();
    }

    @Override
    public void showUserPosition(LatLng position) {
        mPosition = position;
        addUserMarker();
        if (mOldCenter == null)
            moveCamera(position);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMap != null) {
            outState.putParcelable(MAP_OLD_CENTER, mMap.getCameraPosition().target);
            outState.putFloat(MAP_OLD_ZOOM, mMap.getCameraPosition().zoom);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.stop();
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    private void addUserMarker() {
        if(mPosition == null)
            return;

        mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .position(mPosition))
                .setTitle(getString(R.string.user_position));
    }

    private void addStationMarkerIfNotPresent(Station station) {
        if (mVisibleStations.containsKey(station.getId()))
            return;

        Marker marker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                .position(station.getLatLng())
                .title(getString(R.string.marker_station_title, station.getId()))
                .snippet(getString(R.string.marker_station_snippet, station.getLines()))
        );
        marker.setTag(station);
        mMap.setOnInfoWindowClickListener(mPresenter);
        mVisibleStations.put(station.getId(), marker);
    }

    private void removeStationMarker(Station station) {
        Marker marker = mVisibleStations.remove(station.getId());
        if (marker != null)
            marker.remove();
    }

    private void moveCamera(LatLng newPosition) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(newPosition));
        updateMarkers();
    }

    private void updateMarkers() {
        if (mStations == null)
            return;

        LatLngBounds visibleBounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        for (Station station : mStations) {
            if (visibleBounds.contains(station.getLatLng()))
                addStationMarkerIfNotPresent(station);
            else {
                removeStationMarker(station);
            }
        }
    }

}
